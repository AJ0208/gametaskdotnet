﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace Snake
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// Adding image controls to the game grid
        /// </summary>
        /// 

        private readonly Dictionary<GridValue, ImageSource> gridValToImage = new()
        {
            {GridValue.Empty , Images.Empty },
            {GridValue.Snake , Images.Body },
            {GridValue.Food , Images.Food}

        };

        private readonly Dictionary<Direction, int> directionToDirection = new()
        {
            {Direction.Up, 0},
            {Direction.Right, 90},
            {Direction.Down, 180},
            {Direction.Left, 270},
        };


        //taking variables for rows & columns
        private readonly int rows = 15;
        private readonly int columns = 15;
        //access the image for the given position
        private readonly Image[,] gridImages;
        private GameState gameState;
        private bool gameRunning; // it is false bydefault

        public MainWindow()
        {
            InitializeComponent();
            gridImages = SetupGrid();
            gameState = new GameState(rows, columns);

        }

        //when user press the key first time we call runGame
        private async Task RunGame()
        {
            Draw();
            await ShowCountDown();
            Overlay.Visibility = Visibility.Hidden;
            await GameLoop();
            await ShowGameOver();
            gameState = new GameState(rows, columns); //for new game state
        }


        //when user presses the kry then PreviewKeyDown is called & after that Window_KeyDown is also called
        private async void Window_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            //if overlay visible the event handle prpperty is true
            if (Overlay.Visibility == Visibility.Visible)
            {
                e.Handled = true;
            }

            //if game is not already running
            if (!gameRunning)
            {
                gameRunning = true;
                await RunGame();
                gameRunning = false;
            }

        }

        //when user presses the key this method is called
        private void Window_KeyDown(Object sender, KeyEventArgs e)
        {
            //if game is over it will return nothing
            if (gameState.GameOver)
            {
                return;
            }

            //otherwise check which key is pressed
            switch (e.Key)
            {
                case Key.Left:
                    gameState.ChangeDirection(Direction.Left);
                    break;

                case Key.Right:
                    gameState.ChangeDirection(Direction.Right);
                    break;

                case Key.Up:
                    gameState.ChangeDirection(Direction.Up);
                    break;

                case Key.Down:
                    gameState.ChangeDirection(Direction.Down);
                    break;
            }
        }

        //** we need to move snake at regular intervals
        private async Task GameLoop()
        {
            while (!gameState.GameOver)
            {
                //add small delay
                await Task.Delay(500);
                gameState.Move();
                Draw();
            }
        }


        //it will add the required image controls to the game grid and return them in 2d array
        private Image[,] SetupGrid()
        {
            Image[,] images = new Image[rows, columns];
            GameGrid.Rows = rows;
            GameGrid.Columns = columns;
            GameGrid.Width = GameGrid.Height * (columns / (double)rows);

            for (int r = 0; r < rows; r++)
            {
                for (int c = 0; c < columns; c++)
                {
                    Image image = new()
                    {
                        Source = Images.Empty,
                        RenderTransformOrigin = new Point(0.5, 0.5)
                    };

                    images[r, c] = image;
                    GameGrid.Children.Add(image);
                }

            }
            return images;
        }

        // call DrawGrid from more general draw method
        private void Draw()
        {
            //calling DrawGrid() &   DrawSnakeHead()
            DrawGrid();
            DrawSnakeHead();

            //we set the scoreTtext to Score follwed by the actual score stalled in the game state
            ScoreText.Text = $"SCORE{gameState.Score}";
        }

        //** look at the grid array in the game state & update the grid images to reflect it
        private void DrawGrid()
        {
            for (int r = 0; r < rows; r++)
            {
                for (int c = 0; c < columns; c++)
                {
                    //get the grid value at the current position
                    GridValue gridValue = gameState.Grid[r, c];

                    //set the source for the corresponding image using our dictionar
                    gridImages[r, c].Source = gridValToImage[gridValue];
                    gridImages[r, c].RenderTransform = Transform.Identity; //rotate image is the one with the snkae's head
                }
            }
        }

        //draw snake head
        private void DrawSnakeHead()
        {
            //get the position of the snake head & rid position
            Position headPosition = gameState.HeadPosition();
            Image image = gridImages[headPosition.Row, headPosition.Column];
            image.Source = Images.Head;

            int rotation = directionToDirection[gameState.DirectionSnake];
            image.RenderTransform = new RotateTransform(rotation);

        }

        private async Task DrawDeadSnake()
        {
            List<Position> positions = new(gameState.SnakePositions());
            for (int i = 0; i < positions.Count; i++)
            {
                Position pos = positions[i];
                ImageSource source = (i == 0) ? Images.DeadHead : Images.DeadBody;
                gridImages[pos.Row, pos.Column].Source = source;
                await Task.Delay(50);
            }

        }



        //we are adding the countDown to game to start
        private async Task ShowCountDown()
        {
            for (int i = 3; i >= 1; i--)
            {
                OverlayText.Text = i.ToString();
                await Task.Delay(100);
            }
        }


        //method to restart the game
        private async Task ShowGameOver()
        {
            await DrawDeadSnake();
            await Task.Delay(100);
            Overlay.Visibility = Visibility.Visible;
            OverlayText.Text = "PRESS ANY KEY TO START";
        }
    }
}
