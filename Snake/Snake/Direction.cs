﻿
using System;
using System.Collections.Generic;

namespace Snake
{
    public  class Direction
    {
        //static variables --need only 4 direction 
        public readonly static Direction Left = new(0,-1);              //move left from the given position must leave row unchanged & subtract one column
        public readonly static Direction Right = new(0,1);              //move right don't change row but add one column     
        public readonly static Direction Up = new(-1,0);                //move up subtract one row & don't change the column
        public readonly static Direction Down = new(1,0);               //move down add one row  & don't change the column

        //creating 2 variables for row and column
        public int RowOffset { get; }
        public int ColumnOffset { get; }

        // private constructor --> no other class create the instance of direction class
        private Direction(int rowOffSet , int columnOffSet) 
        {
            RowOffset = rowOffSet;
            ColumnOffset = columnOffSet;
        }


        //create message as opposite
        public Direction Opposite()
        {
            return new Direction(-RowOffset, -ColumnOffset);
        }



        //ovrride equals , getHashCode so direction class can be passed as dictionary

        public override bool Equals(object obj)
        {
            return obj is Direction direction &&
                   RowOffset == direction.RowOffset &&
                   ColumnOffset == direction.ColumnOffset;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(RowOffset, ColumnOffset);
        }

        public static bool operator ==(Direction left, Direction right)
        {
            return EqualityComparer<Direction>.Default.Equals(left, right);
        }

        public static bool operator !=(Direction left, Direction right)
        {
            return !(left == right);
        }

    }
}
