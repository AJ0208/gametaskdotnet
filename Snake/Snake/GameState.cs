﻿using System;
using System.Collections.Generic;

namespace Snake
{
    public  class GameState
    {
        //num of rows columns in the grid
        public int Rows { get;  }
        public int Columns { get; }

        // 2d rectangular array
        public GridValue[,] Grid { get; }

        //snake direction where that can move next
        public Direction DirectionSnake { get; private set; }

        //2d array for the grid value
       // public GridValue[,] GridValues { get; }

        //score
        public int  Score { get; private set; }

        //gameOver
        public bool GameOver { get; private set; }


        //to keep a list the current positions occupied by the snake using linkedlist to add & remove from both the ends
        private readonly LinkedList<Position>snakePositions= new();

        //taking random object to figure out where food should spawn
        private  readonly Random random= new();


        //add variable for the buffer
        private readonly LinkedList<Direction> directionChanges = new ();


        //** creating constructor
        public GameState(int rows, int columns)
        {
            Rows = rows;
            Columns = columns;

            //intialize 2d array with the size  --- every position of 2d array  contain gridvalue.empty because it's first enum value
            Grid = new GridValue[rows, columns];

            //starting direction of snake  ---  when game start snake direction should be right
            DirectionSnake = Direction.Right;

            //calling AddSnake() method from constructor
            AddSnake();

            //calling AddFood() method from constructor
            AddFood();
        }


        //** add the snake to the grid
        private void AddSnake()
        {
            //crating variable for middle row
            int r = Rows / 2;
            for (int c = 1; c <= 3; c++)
            {
                //grid entry
                Grid[r, c] = GridValue.Snake;
                //add the position to the snakePositions list 
                snakePositions.AddFirst(new Position(r, c));
            }
        }


        //** return all empty grid positions
        private IEnumerable<Position> EmptyPositions()
        {
            for (int r = 0; r < Rows; r++)
            {
                for (int c = 0; c < Columns; c++)
                {
                    if (Grid[r, c] == GridValue.Empty)
                    {
                        yield return new Position(r, c);
                    }
                }
            }
        }

        //** add the food in the grid
        private void AddFood()
        {
            //create a list of empty positions
            List<Position> empty = new(EmptyPositions());

            //if there are no empty positions
            if (empty.Count == 0)
            {
                return;
            }

            //pick the empty position at random
            Position position = empty[random.Next(empty.Count)];
            //& set the corresponding array entry to GridValue.food
            Grid[position.Row, position.Column] = GridValue.Food;
        }

        //-------------- Helping method for the snake  ---------------

        //** position for the next head
        public Position HeadPosition()
        {
            //will get the position from the linked list
            return snakePositions.First.Value;
        }

        //** similar for the tail position
        public Position TailPosition()
        {
            //will get the position from the end of the linked list
            return snakePositions.Last.Value;
        }

        //** return snake positions as an eye innumerable 
        public IEnumerable<Position> SnakePositions()
        {
            return snakePositions;
        }

        //---------- Methods for modifying the snake ------------

        //** add the given position to the front of the snake making new head
        private void AddHead(Position position)
        {
            snakePositions.AddFirst(position);
            Grid[position.Row, position.Column] = GridValue.Snake;
        }

        //** method for removing the tail
        private void RemoveTail()
        {
            Position tail = snakePositions.Last.Value;
            Grid[tail.Row, tail.Column] = GridValue.Empty;
            snakePositions.RemoveLast();
        }

        //get the last direction
        private Direction GetLastDirection() //return snake last predetermined direction
        {
            //if buffer is empty
           if(directionChanges.Count == 0)
            {
                return DirectionSnake;
            }
           return directionChanges.Last.Value;
        }

        //return true if given direction added to the buffer otherwise false
        private bool CanChangeDirection(Direction newDirection)
        {
            //if alredy changed direction added to the buffer so return false
            if(directionChanges.Count == 2)
            {
                return false;
            }

            // if there is space in the buffer we will get last predetermined direction
            Direction lastDirection = GetLastDirection();

            //return true if newDirection is not same as last direction
            return newDirection != lastDirection && newDirection != lastDirection.Opposite();
         }


        public void ChangeDirection(Direction direction)
        {
           //if can change direction
           if(CanChangeDirection(direction))
            {
                directionChanges.AddLast(direction);
            }
        }

        //** method to check the given position is outside thr grid or not
        private bool OutsideGrid(Position position)
        {
           return position.Row < 0 || position.Row >= Rows || position.Column < 0 || position.Column >= Columns;
        }


        //** return what is stored in the grid at that position --- what snake would hit if it move there
        private GridValue WillHit(Position newHeadPosition)
        {

            //if new headposition is outside the grid
            if (OutsideGrid(newHeadPosition))
            {
                return GridValue.Outside;
            }

            //check the new head position is same as the tail position -- game should end in the case

            if (newHeadPosition == TailPosition())
            {
                return GridValue.Empty;
            }
            return Grid[newHeadPosition.Row, newHeadPosition.Column];
        }

        //** move snake one step in the current direction
        public void Move()
        {
            //check if there is direction change in buffer we change the dirction of snake accrodingly
            //& remove the direction from the buffer 
            if(directionChanges.Count > 0) 
            {
                DirectionSnake = directionChanges.First.Value;
                directionChanges.RemoveLast();


            }

            // first get the new head position
            Position newHeadPosition = HeadPosition().Translate(DirectionSnake);
            // then chaeck what the head will hit
            GridValue hit = WillHit(newHeadPosition);

            //if hit GridValue.Outside or GridValue.Snake the game over
            if (hit == GridValue.Outside || hit == GridValue.Snake)
            {
                GameOver = true;
            }

            //if snake move into an empty position we remove current tail & addnew head
            else if (hit == GridValue.Empty)
            {
                RemoveTail();
                AddHead(newHeadPosition);
            }

            //if it hits the position with food then don't remove the tail and add the new head & increment the score & spawn the food somwhere else
            else if (hit == GridValue.Food)
            {
                AddHead(newHeadPosition);
                Score++;
                AddFood();
            }
        }
    }
}
