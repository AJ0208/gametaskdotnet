﻿
namespace Snake
{
    public enum GridValue
    {
        Empty,          //position of the grid can be empty
        Snake,          //it can contain part of the snake
        Food,           //it can contain food
        Outside         //won't srored in grid array but convienient in the code when snake want to move outside the grid
    }
}
